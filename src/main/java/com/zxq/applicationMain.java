package com.zxq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.zxq.mapper")
@EnableCaching
@EnableScheduling
public class applicationMain {
    public static void main(String[] args) {
        SpringApplication.run(applicationMain.class, args);
    }
}
