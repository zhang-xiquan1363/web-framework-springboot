package com.zxq.controller;

import com.zxq.dao.UserDao;
import com.zxq.domain.User;
import com.zxq.service.LoginLogService;
import com.zxq.service.RegisterLogService;
import com.zxq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;
    @Autowired
    private RegisterLogService registerLogService;
    @Autowired
    private LoginLogService loginLogService;

    private StringRedisTemplate redisTemplate;



    @RequestMapping("/login")
    public String loginControl(String username,String password) throws UnknownHostException {
        String findedUser = userService.loginService2(username, password);
        if("登录成功".equals(findedUser)){
            //更新用户表最后登录ip和最后登录时间
            return "success";
        }else{
            System.out.println(findedUser);
        }
        return findedUser;
    }

    @RequestMapping("/register")
    public String registerControl(String username, String password) throws UnknownHostException {
        boolean registerSuccess = userService.registerService(username, password);
        if(registerSuccess) {
            System.out.println("注册成功，请登录吧！");
            //添加完用户后，添加到日志
            registerLogService.addRegisterLog(username);
            return "index";
        }
        return "index";
    }

}
