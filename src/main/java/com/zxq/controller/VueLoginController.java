package com.zxq.controller;

import com.zxq.domain.Result;
import com.zxq.domain.VueLoginDTO;
import com.zxq.service.VueLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VueLoginController {

    @Autowired
    private VueLoginService vueLoginService;

    @PostMapping(value = "/api/login")
    @CrossOrigin
    public Result login(@RequestBody VueLoginDTO vueLoginDTO) {
        return vueLoginService.login(vueLoginDTO);
    }
}
