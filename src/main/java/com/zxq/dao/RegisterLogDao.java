package com.zxq.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class RegisterLogDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void creatRegisterLog(String username){
        String sql = "INSERT INTO register_log(username, ip, register_time) VALUES(?,?,?)";
        Object[] args = {username, "127.0.0.1", new Date()};
        jdbcTemplate.update(sql, args);
    }
}
