package com.zxq.dao;

import com.zxq.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
@Mapper
public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    /**
     *  通过用户名判断用户是否存在
     * @param username
     * @return 查询结果
     */
    public boolean getUserByUsername(String username){
        String sql = "SELECT username,password FROM t_user WHERE username = ?";
        Object[] args = {username};
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, args);
        System.out.println(maps);
//        String username1 = (String) maps.get(0).get("username");
//        String password = (String) maps.get(0).get("password");
//        System.out.println(username1);
//        System.out.println(password);
        if(maps.size() == 0){
            return false;
        }
        return true;
    }
    /**
     * 验证用户的密码是否正确 登录时使用
     * @param username
     * @param password
     * @return
     */
    public boolean getUserByUserNameAndPassword(String username, String password){
        String sql = "SELECT username,password FROM t_user WHERE username = ? AND password = ?";
        Object[] args = {username, password};
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, args);
        System.out.println(maps);

        if(maps.size() == 0){
            return false;
        }
        return true;
    }
    /**
     * 登录成功后，更新用户表中用户的登录ip的最后登录时间
     * @param username
     */
    public void updateUserDao(String username) {
        String sql = "UPDATE t_user SET last_ip = ?, last_time = ? WHERE username = ?";
        String ip = "127.0.0.1";
        Date time = new Date();
        Object[] args = {ip, time, username};
        jdbcTemplate.update(sql, args);
    }

    /**
     * 注册时使用
     * @param username
     * @param password
     */
    public void registerDao(String username, String password){
        String sql = "INSERT INTO t_user(username, password) values (?,?)";
        Object[] args = {username, password};
        jdbcTemplate.update(sql, args);
    }


}
