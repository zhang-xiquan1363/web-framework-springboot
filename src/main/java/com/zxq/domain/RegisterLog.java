package com.zxq.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterLog {
    @TableId(value = "register_log_id", type = IdType.AUTO)
    private Integer register_log_id;
    private String username;
    private String ip;
    private Date registerTime;
}
