package com.zxq.domain;


import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "user")
public class User {
    //用户的属性
//    private int user_id;
    @TableId(value = "user_id",type = IdType.AUTO)
    private Integer userId;
    private String username;
    private String password;

    private String lastIp;
    //测试是否可以成功
    //第二次测试
    //测试第三次


    //测试NewBranch

    private Date lastTime;

    private Integer successCount;

    private Integer failureCount;
}
