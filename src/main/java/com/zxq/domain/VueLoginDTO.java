package com.zxq.domain;

import lombok.Data;

@Data
public class VueLoginDTO {
    private String username;
    private String password;
}
