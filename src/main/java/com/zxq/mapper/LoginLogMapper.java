package com.zxq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxq.domain.LoginLog;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginLogMapper extends BaseMapper<LoginLog> {
}
