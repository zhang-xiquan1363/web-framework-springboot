package com.zxq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxq.domain.RegisterLog;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterLogMapper extends BaseMapper<RegisterLog> {
}
