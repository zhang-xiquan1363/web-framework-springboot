package com.zxq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxq.domain.User;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserMapper extends BaseMapper<User> {

    User queryByName(String username);

    User queryByNamePassword(String username, String password);

    void insertStatistic(Date statistic_date, String username, Integer success_count, Integer failure_count);

    void resetLoginCounts();

}
