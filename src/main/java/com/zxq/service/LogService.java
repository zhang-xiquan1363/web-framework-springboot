package com.zxq.service;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.zxq.domain.User;
import com.zxq.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class LogService {


    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private UserMapper userMapper;

    // cron

    /**
     * 位置   时间域     允许值
     * 1     秒         0-59
     * 2     分         0-59
     * 3     小时        0-23
     * 4     日期        1-31
     * 5     月份        1-12
     * 6     星期        1-7
     *
     *  * 所有字段，每个
     *  ？ 占位符  在日期和星期中
     *  -  表示范围
     */
    @Scheduled(cron = "0 16 17 * * ?")//每天凌晨2点开启
    public void reportCurrentTime(){

        List<User> users = userMapper.selectList(null);

        for (User u: users) {
            System.out.println("用户 " +u.getUsername() +
                    " : 今天的登录成功次数为" + u.getSuccessCount() +
                    " 。 登录失败次数为" + u.getFailureCount());
            userMapper.insertStatistic(new Date(), u.getUsername(), u.getSuccessCount(), u.getFailureCount());
        }

        userMapper.resetLoginCounts();

    }



}
