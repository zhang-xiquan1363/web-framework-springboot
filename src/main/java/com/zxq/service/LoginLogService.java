package com.zxq.service;

import com.zxq.domain.LoginLog;
import com.zxq.mapper.LoginLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;

@Service
public class LoginLogService {
    @Autowired
    private LoginLogMapper loginLogMapper;

    public void addLoginLog(String username) throws UnknownHostException {
        LoginLog loginLog = new LoginLog();
        loginLog.setUsername(username);
        loginLog.setLoginTime(new Date());
//        loginLog.setIp(Inet4Address.getLocalHost().getHostAddress());
        loginLog.setIp(Inet4Address.getLocalHost().getHostAddress());
        loginLogMapper.insert(loginLog);
    }
}
