package com.zxq.service;

import com.zxq.domain.RegisterLog;
import com.zxq.mapper.RegisterLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;

@Service
public class RegisterLogService {

    @Autowired
    private RegisterLogMapper registerLogMapper;

    public void addRegisterLog(String username) throws UnknownHostException {
        RegisterLog registerLog = new RegisterLog();
        registerLog.setUsername(username);
        registerLog.setRegisterTime(new Date());
//        registerLog.setIp(Inet4Address.getLocalHost().getHostAddress());
        registerLog.setIp(Inet4Address.getLocalHost().getHostAddress());
        registerLogMapper.insert(registerLog);
    }

}
