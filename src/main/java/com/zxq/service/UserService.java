package com.zxq.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zxq.dao.LoginLogDao;
import com.zxq.dao.RegisterLogDao;
import com.zxq.dao.UserDao;
import com.zxq.domain.User;
import com.zxq.mapper.LoginLogMapper;
import com.zxq.mapper.RegisterLogMapper;
import com.zxq.mapper.UserMapper;
import org.slf4j.helpers.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.net.util.IPAddressUtil;
import sun.security.x509.IPAddressName;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserService {
    //spring jdbc 注销，暂不使用
//    @Autowired
//    private UserDao userDao;
//    @Autowired
//    private RegisterLogDao registerDao;
//    @Autowired
//    private LoginLogDao loginDao;
    @Autowired
    private UserService userService;
    @Autowired
    private RegisterLogService registerLogService;
    @Autowired
    private LoginLogService loginLogService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private LoginLogMapper loginLogMapper;

    @Autowired
    private RegisterLogMapper registerLogMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

//    时间间隔
    private static final int TIME_INTERVAL = 10;
//    登录失败重试次数上限
    private static final int FALED_RETRY_TIMES = 3;
//    redis记录用户登录失败次数key
    public static final String USER_LOGIN_FAILED_COUNT = "USER:LOGIN:FAILED:COUNT:";

    /**
     * 根据key获取计数器
     *
     * @param key key
     * @return
     */
    private RedisAtomicInteger getRedisCounter(String key) {
        RedisAtomicInteger counter =
                new RedisAtomicInteger(key, stringRedisTemplate.getConnectionFactory());
        //这一段代码好像可以没有
        if (counter.get() == 0) {
            // 设置过期时间，10分钟
            counter.expire(TIME_INTERVAL, TimeUnit.MINUTES);
        }
        return counter;
    }
    /**
     *  登录服务
     *
     * @param username 用户输入的用户名
     * @param password 用户输入的密码
     * @return 数据库中有匹配的用户信息则返回，否则返回 null
     */
    @Transactional
    public String loginService2(String username, String password) throws UnknownHostException {
        //key保存到redis
        String key = USER_LOGIN_FAILED_COUNT + username;
        System.out.println(key);
        RedisAtomicInteger counter = getRedisCounter(key);

        //输错三次后锁定一分钟
        if(counter.get() >= FALED_RETRY_TIMES) {
            stringRedisTemplate.expire(key, 1, TimeUnit.MINUTES);
            return "登录失败次数已达到上限，请稍后再试。";
        }
        User user = userMapper.queryByName(username);
        if(user == null){
            //user为空，说明用户名不存在
            return "用户不存在";
         }
        //用户存在，看用户的用户名与密码是否正确
        user = userMapper.queryByNamePassword(username,password);

        if(user == null){
            //user为空，说明用户的密码错误，需要给错误登录次数 + 1
            counter.getAndIncrement();
            loginFailureProcess(username);
            return "登录失败" + counter.get() + "次";
        }
        //更新用户信息
        userService.updateUserSuccessInfo(user);
        //添加登录日志
        loginLogService.addLoginLog(username);
        //登录成功，清楚缓存中的key
        stringRedisTemplate.delete(key);
        return "登录成功";
    }
    //不用了
//    public boolean loginService(String username, String password){
//
//        //判断是否存在 用户名 密码 匹配的用户
//        boolean validSuccess = userDao.getUserByUserNameAndPassword(username,password);
//        if(validSuccess == false){
//            return false;
//        }
//        //登录成功，创建登录日志
//        userDao.updateUserDao(username);
//        loginDao.creatLoginLog(username);
//        return true;
//    }

    /**
     * 注册服务
     * @param username 注册的用户名
     * @param password 注册的密码
     * @return 注册是否成功
     */
    @Transactional
    public boolean registerService(String username, String password){
        //存在用户，返回false
        User user = userMapper.queryByName(username);
        if(user == null) {
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setSuccessCount(0);
            user.setFailureCount(0);
            userMapper.insert(user);
            return true;
        }
        return false;
    }

    /**
     * 登录成功后，更新用户表中登录成功次数
     * @param user
     * @throws UnknownHostException
     */
    @Transactional
    public void updateUserSuccessInfo(User user) throws UnknownHostException{
        user.setLastIp(Inet4Address.getLocalHost().getHostAddress());
        user.setLastTime(new Date());
        user.setSuccessCount(user.getSuccessCount() + 1);
        userMapper.updateById(user);
    }

    /**
     * 登录失败后，更新用户表中登录失败次数
     * @param username 用戶名
     */
    @Transactional
    public void loginFailureProcess(String username) {
        User user1 = userMapper.queryByName(username);
        user1.setFailureCount(user1.getFailureCount() + 1);
        userMapper.updateById(user1);

    }
}
