package com.zxq.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zxq.domain.Result;
import com.zxq.domain.User;
import com.zxq.domain.VueLoginDTO;
import com.zxq.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VueLoginService {
    @Autowired
    private UserMapper userMapper;


    public Result login(VueLoginDTO loginDTO) {
        if (StringUtils.isEmpty(loginDTO.getUsername())){
            return new Result(400,"账号不能为空","");
        }
        if (StringUtils.isEmpty(loginDTO.getPassword())){
            return new Result(400,"密码不能为空","");
        }
        //通过登录名查询用户
        QueryWrapper<User> wrapper = new QueryWrapper();
        wrapper.eq("username", loginDTO.getUsername());
        User uer=userMapper.selectOne(wrapper);
        //比较密码
        if (uer!=null&&uer.getPassword().equals(loginDTO.getPassword())){
            return new Result(200,"",uer);
        }
        return new Result(400,"登录失败","");
    }
}
