package com.zxq.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zxq.domain.User;
import com.zxq.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SpringBootTest
public class mapperTest {

    @Autowired
    private UserMapper userMapper;



    @Test
    public void testFindAll(){
        List<User> users= userMapper.selectList(null);
        System.out.println(users);

    }

    @Test
    @Transactional
    public void testFindOne(){

        String username = "zxq123412";

        User user = userMapper.queryByName(username);
        System.out.println(user);
    }

    @Test
    public void testFindByName(){
        String name = "zxq134";
        Map<String, Object> maps = new HashMap<>();
        maps.put("username", name);

        QueryWrapper queryWrapper = new QueryWrapper();
        QueryWrapper<User> queryWrapper2 = new QueryWrapper<>();
        //判断用户是否存在
        queryWrapper.allEq(maps);

        queryWrapper2.eq("username", name);

        User user = userMapper.selectOne(queryWrapper2);

        System.out.println(user);

        List<Map<String, Object>> findedUsers = userMapper.selectMaps(queryWrapper);

        System.out.println(findedUsers);

    }

    @Test
    public void testLogin() throws UnknownHostException {
        String username = "zxq";
        String password = "zxq";
        User user = userMapper.queryByNamePassword(username, password);
        System.out.println(user);
    }


}
